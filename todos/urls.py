from django.urls import path
from todos.views import (
    todoList,
    todo_list_detail,
    new_todo,
    edit_todo,
    delet_todo,
    update_todo_item,
    edit_todo_item,
)

urlpatterns = [
    path("", todoList, name="todo_list_list"),
    path("<int:id>/", todo_list_detail, name="todo_list_detail"),
    path("create/", new_todo, name="todo_list_create"),
    path("<int:id>/edit/", edit_todo, name="todo_list_update"),
    path("<int:id>/delete/", delet_todo, name="todo_list_delete"),
    path("items/create/", update_todo_item, name="todo_item_create"),
    path("items/<int:id>/edit/", edit_todo_item, name="todo_item_update"),
]
