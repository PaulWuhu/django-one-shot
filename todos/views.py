from django.shortcuts import get_object_or_404, render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


def todoList(request):
    todolist = TodoList.objects.all()
    context = {"todo_list": todolist}
    return render(request, "todos/todolist.html", context)


def todo_list_detail(request, id):
    todoitems = get_object_or_404(TodoList, id=id)
    context = {"todo_object": todoitems}
    return render(request, "todos/todoitem.html", context)


def new_todo(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            newlist = form.save()
            form.save
            return redirect("todo_list_detail", id=newlist.id)
    else:
        form = TodoListForm()
    context = {"form": form}
    return render(request, "todos/newtodo.html", context)


def delet_todo(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


# this is my feature 12
def update_todo_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            newitem = form.save()
            return redirect("todo_list_detail", id=newitem.list.id)
    else:
        form = TodoItemForm()
    context = {"form": form}
    return render(request, "todos/newtodoitem.html", context)


# this is my feature 13
def edit_todo_item(request, id):
    todo_item_list = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item_list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=todo_item_list.list.id)
    else:
        form = TodoItemForm(instance=todo_item_list)
    context = {"todo_item_list_object": todo_item_list, "form": form}
    return render(request, "todos/ubdatetodoitem.html", context)


def edit_todo(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            newlist = form.save()
            return redirect("todo_list_detail", id=newlist.id)
    else:
        form = TodoListForm(instance=todo_list)
    context = {"form": form, "todo_object": todo_list}
    return render(request, "todos/updatetodo.html", context)
